package com.ikaiwa.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ikaiwa.backend.domain.Seito;

@Repository
public interface SeitoRepository extends JpaRepository<Seito,Long>{
    @Query(value = "FROM Seito s Where s.user.id = ?1 ")
    Seito findSeitoByUserId(Long id);

}
