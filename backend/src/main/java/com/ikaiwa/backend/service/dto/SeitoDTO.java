package com.ikaiwa.backend.service.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeitoDTO {

    private Long id;

    private String name;

    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(max = 11)
    private String phone;

    @NotBlank
    @Size(max = 3)
    private String level;

    private int money;

    private int warningCount;
    
}
