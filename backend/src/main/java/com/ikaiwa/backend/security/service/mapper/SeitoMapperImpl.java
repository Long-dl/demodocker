package com.ikaiwa.backend.security.service.mapper;

import org.springframework.stereotype.Service;

import com.ikaiwa.backend.domain.Seito;
import com.ikaiwa.backend.service.dto.SeitoDTO;

@Service
public class SeitoMapperImpl implements SeitoMapper{

    @Override
    public SeitoDTO seitoToSeitoDTo(Seito seito) {
        if(seito == null){
            return null;
        }
        SeitoDTO seitoDTO = new SeitoDTO();
        seitoDTO.setId(seito.getId());
        seitoDTO.setName(seito.getUser().getName());
        seitoDTO.setEmail(seito.getUser().getEmail());
        seitoDTO.setUsername(seito.getUser().getUsername());
        seitoDTO.setLevel(seito.getLevel());
        seitoDTO.setMoney(seito.getMoney());
        seitoDTO.setPhone(seito.getPhone());
        seitoDTO.setWarningCount(seito.getWarningCount());
        return seitoDTO;
    }

    @Override
    public Seito seitoDtoToSeito(SeitoDTO seitoDTO, Seito seito) {
        if(seitoDTO == null){
            return null;
        }
        seito.getUser().setEmail(seitoDTO.getEmail());
        seito.setPhone(seitoDTO.getPhone());
        seito.setLevel(seitoDTO.getLevel());
        return seito;
    }
    
}
