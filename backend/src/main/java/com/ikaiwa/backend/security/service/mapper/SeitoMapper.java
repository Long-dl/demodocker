package com.ikaiwa.backend.security.service.mapper;

import org.mapstruct.Mapper;

import com.ikaiwa.backend.domain.Seito;
import com.ikaiwa.backend.service.dto.SeitoDTO;

@Mapper 
public interface SeitoMapper {
    SeitoDTO seitoToSeitoDTo(Seito seito);
    Seito seitoDtoToSeito(SeitoDTO seitoDTO, Seito seito);
}
