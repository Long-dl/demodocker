package com.ikaiwa.backend.payload.response;

import lombok.Data;

@Data
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String name;
    
    public JwtResponse(String token, Long id, String name) {
        this.token = token;
        this.id = id;
        this.name = name;
    }

}
