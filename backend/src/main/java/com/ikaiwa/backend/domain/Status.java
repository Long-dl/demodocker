package com.ikaiwa.backend.domain;

public enum Status {
    OPEN,
    WORKING,
    DONE
}
