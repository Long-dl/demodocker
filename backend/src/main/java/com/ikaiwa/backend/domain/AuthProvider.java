package com.ikaiwa.backend.domain;

public enum AuthProvider {
    LOCAL,
    GOOGLE
}
