package com.ikaiwa.backend.domain;

public enum ERole {
    ROLE_SEITO,
    ROLE_SENSEI,
    ROLE_ADMIN,
    ROLE_WEBOWNER
}
