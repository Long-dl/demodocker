package com.ikaiwa.backend.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "user", uniqueConstraints = { @UniqueConstraint(columnNames = "username") })
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank
  @Size(max = 20)
  @Column(name = "username")
  private String username;

  private String name;

  @NotBlank
  @Email
  private String email;

  private String imageUrl;

  @NotBlank
  @JsonIgnore
  private Boolean emailVerified = false;

  @NotBlank
  @Size(max = 120)
  @Column(name = "password")
  private String password;

  @NotBlank
  @JsonIgnore
  @Enumerated(EnumType.STRING)
  private AuthProvider provider;

  @JsonIgnore
  private String providerId;

  @ManyToOne
  @JoinColumn(name = "roles_id", nullable = false)
  private Role roles;

  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
  private Seito seito;

  public User(String username, String name, String email,
      String password) {
    this.username = username;
    this.name = name;
    this.email = email;
    this.password = password;
  }
}
