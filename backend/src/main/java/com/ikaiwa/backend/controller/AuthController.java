package com.ikaiwa.backend.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ikaiwa.backend.domain.AuthProvider;
import com.ikaiwa.backend.domain.ERole;
import com.ikaiwa.backend.domain.Role;
import com.ikaiwa.backend.domain.Seito;
import com.ikaiwa.backend.domain.User;
import com.ikaiwa.backend.exception.BadRequestException;
import com.ikaiwa.backend.payload.request.LoginRequest;
import com.ikaiwa.backend.payload.request.SigupRequest;
import com.ikaiwa.backend.payload.response.JwtResponse;
import com.ikaiwa.backend.payload.response.MessageResponse;
import com.ikaiwa.backend.repository.RoleRepository;
import com.ikaiwa.backend.repository.SeitoRepository;
import com.ikaiwa.backend.repository.UserRepository;
import com.ikaiwa.backend.security.jwt.JwtUtils;
import com.ikaiwa.backend.security.service.UserDetailsImpl;

@RestController
@RequestMapping("/user")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    SeitoRepository seitoRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal(); 
        return ResponseEntity.ok(new JwtResponse(jwt,userDetails.getId(),userDetails.getName()));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SigupRequest sigupRequest) {
        if (userRepository.existsByUsername(sigupRequest.getUsername())) {
            throw new BadRequestException("username address already in use.");
        }

        User user = new User(sigupRequest.getUsername(),sigupRequest.getName(),sigupRequest.getEmail(), encoder.encode(sigupRequest.getPassword()));
        user.setProvider(AuthProvider.LOCAL);
        Role role = roleRepository.findByName(ERole.ROLE_SEITO).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        user.setRoles(role);
        Seito seito = new Seito();
        userRepository.save(user);
        seito.setPhone(sigupRequest.getPhone());
        seito.setUser(user);
        Seito result = seitoRepository.save(seito);
        //User result = userRepository.save(user);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/seito/{id}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new MessageResponse( "User registered successfully@",true));
    }

}
