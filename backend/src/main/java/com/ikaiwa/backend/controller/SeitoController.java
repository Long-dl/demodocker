package com.ikaiwa.backend.controller;

import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ikaiwa.backend.domain.Seito;
import com.ikaiwa.backend.exception.BadRequestException;
import com.ikaiwa.backend.repository.SeitoRepository;
import com.ikaiwa.backend.security.service.mapper.SeitoMapperImpl;
import com.ikaiwa.backend.service.dto.SeitoDTO;
@RestController
@RequestMapping("/seito")
public class SeitoController {

    @Autowired
    private SeitoRepository seitoRepository;

    @Autowired
    private SeitoMapperImpl seitoMapperImpl;

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('SEITO')")
    public SeitoDTO getCurrentUser(@PathVariable(value = "id", required = false) final Long id) {
        Seito seito = seitoRepository.findSeitoByUserId(id);
        SeitoDTO seitoDTO = seitoMapperImpl.seitoToSeitoDTo(seito);
        return seitoDTO;
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> updateSeito(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody Map<Object, Object> fields)throws URISyntaxException{
        Seito seito = seitoRepository.findSeitoByUserId(id);
        SeitoDTO seitoDTO = seitoMapperImpl.seitoToSeitoDTo(seito);

        if(seitoDTO.getId() == null){
            throw new BadRequestException("Invalid Id");
        }
        if(seitoDTO.getId() != id){
            throw new BadRequestException("Invalid Id");
        }
        if(!seitoRepository.existsById(id)){
            throw new BadRequestException("Entity not fond");
        }   

        fields.forEach((key,value)->{
            Field field = ReflectionUtils.findField(SeitoDTO.class, (String) key);
            field.setAccessible(true);
            ReflectionUtils.setField(field, seitoDTO, value);
        });
        Seito seitoUpdate = seitoMapperImpl.seitoDtoToSeito(seitoDTO, seito);
        seitoRepository.save(seitoUpdate);
        return ResponseEntity.ok("SEITO UPDATED"); 
    }
}
