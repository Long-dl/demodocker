import React from "react";
import SmallHeader from "./SmallHeader.js";
import "./Grammar.css";

function Vocabulary() {
    const examples = [
        {
            id: 0,
            level: 5,
            name: "mãi mà, mãi rồi",
            stucture: " ~なかなか~ない",
            description:
                "~なかなか~ない là phó từ biểu thị mức độ, khi làm chức năng biểu thị nghĩa cho động từ thì sẽ được đặt trước động từ.Diễn tả ý để thực hiện một điều gì đó khá mất thời gian, sức lực và rất khó thực hiện",
            sample: [
                {
                    id: 0,
                    jpSample: "なかなか寝ません。",
                    vnSample: "Mãi mà không ngủ được.",
                },
            ],
        },
        {
            id: 0,
            level: 5,
            name: "Có",
            stucture: "~があります~",
            description:
                "Mẫu câu này dùng để nói về nơi ở, sự hiện hữu của đồ vật. Những vật ở đây sẽ làm chủ ngữ trong câu, và được biểu thị bằng trợ từ「が」「があります」dùng cho đối tượng cố định, không chuyển động được như đồ đạc, cây cối",
            sample: [
                {
                    id: 0,
                    jpSample: "テーブルの上にコンピュータがあります",
                    vnSample: "Có cái máy vi tính trên bàn",
                },
            ],
        },
    ];

    function viewDescrip(description){
        
        console.log(1)
        return(
            <>
            <div>
                {description}
            </div>
            </>
        )
    }

    return (
        <>
        <SmallHeader/>
            <div className="listGrammars">
                {examples.map((example, index) => (
                    <div key={index} className="grammar" onClick={() => viewDescrip()}>
                        <div className="index">{index}</div>
                        <div className="name">{example.name}</div>
                        <div className="stucture">{example.stucture}</div>
                    </div>
                ))}
            </div>
        </>
    );
}

export default Vocabulary;
