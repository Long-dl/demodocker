import React from "react";
import { useNavigate } from "react-router-dom";
import "./HomePage.css"

function HomePage() {
    const navigate = useNavigate()
    const haveUser = true;

    function navToLogin() {
        navigate("/login")
    }
    function navToGrammar() {
        navigate("/grammar")
    }
    function navToRegister() {
        navigate("/register")
    }
    return (
        <div className="body">
        <div className="homePage">
            <div className="area-top">
                <div className="area-imgLogo">
                    <img src={require("..\\public\\Hanase2.png")}></img>
                </div>
                <div className="area-menu">
                    <div className="area-menuTop">
                        <div className="title title-Kaiwa">Kaiwa</div>
                        <div className="title title-Test">Test</div>
                    </div>
                    {haveUser ?
                        <div className="area-user">
                            <div className="icon-user"><i class="fi fi-sr-user"></i></div>
                            <div className="name-user">Welcome </div>
                        </div> :
                        <div className="area-menuBottom">
                            <div className="button-LoginHP" onClick={navToLogin}>Login</div>
                            <div className="button-SignupHP" onClick={navToRegister}>Sign up</div>
                        </div>
                    }
                </div>
            </div>
            <div className="area-bottom">
                <div className="area-features">
                    <div className="btn-feature" >
                        Vocabulary
                    </div>
                    <div className="btn-feature" onClick={navToGrammar}>
                        Grammar
                    </div>
                    <div className="btn-feature">
                        Kanji
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

export default HomePage;
