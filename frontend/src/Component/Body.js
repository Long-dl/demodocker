import React from "react";
import "./Body.css";
import Login from "./Login";
import Grammar from "./Grammar.js"
import { BrowserRouter as Router, Route, Link, NavLink, Routes } from "react-router-dom";
import HomePage from "./HomePage";
import Register from "./Register.js";
import UserDetails from "./UserDetails";


function Body() {
  return (
    <>
          <Routes>
            <Route path="/" element={<HomePage/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/grammar" element={<Grammar/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/userDetail/:id" element={<UserDetails/>}/>
          </Routes>
    </>
  );
}

export default Body
