import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Login.css";

function Register() {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [repassword, setRepassword] = useState();
    const navigate = useNavigate();

    return (
        <div className="body">
        <div className="screenLogin">
            <div className="left">
                <div className="area-logo">
                    <img src={require("..\\public\\Hanase2.png")}></img>
                </div>
            </div>
            <div className="rigth">
                <div className="area-Login">
                    <div className="area-logo">
                        <img src={require("..\\public\\Artboard 3.png")}></img>
                    </div>
                    <div></div>
                    <div className="form-Login">
                        <form>
                            <div className="area-Input">
                                <div className="field">User name:</div>
                                <input
                                    className="inputField"
                                    type={"text"}
                                    onChange={(username) => setUsername(username.target.value)}
                                ></input>
                            </div>
                            <div className="area-Input">
                                <div className="field">Password:</div>
                                <input
                                    className="inputField"
                                    type={"password"}
                                    onChange={(password) => setPassword(password.target.value)}
                                ></input>
                            </div>
                            <div className="area-Input">
                                <div className="field">Re-Password:</div>
                                <input
                                    className="inputField"
                                    type={"password"}
                                    onChange={(repassword) =>
                                        setRepassword(repassword.target.value)
                                    }
                                ></input>
                            </div>
                            <div className="area-status">
                                {password ? (
                                    <div className="right">*Fill field Password</div>
                                ) : (
                                    <div className="warning">*Fill field Password</div>
                                )}
                                {username ? (
                                    <div className="right">*Fill field Username</div>
                                ) : (
                                    <div className="warning">*Fill field Username</div>
                                )}
                                {password ? (
                                    <div className="right">*Fill field Confirm Password</div>
                                ) : (
                                    <div className="warning">*Fill field Confirm Password</div>
                                )}
                                {password != repassword ? (
                                    <div className="warning">*Password No Match</div>
                                ) : (
                                    <div div className="right">*Password Match</div>
                                )}
                            </div>
                            <div className="area-btnLogin">
                                {username && password == repassword ?
                                    <div className="button-Regiter">Register</div>
                                    : <div className="button-RegiterDisable">Register</div>
                                }
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

export default Register;
