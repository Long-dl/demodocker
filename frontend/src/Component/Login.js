import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Login.css";
import ApiLogin from "../Api/ApiUsers/ApiLogin";


function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [statusLogin, setStatusLogin] = useState(true);
    const navigate = useNavigate()

    async function loginSystem() {
        if (ApiLogin(username, password) == 200) {
            navigate("/", { replace: true })
        }
        else { setStatusLogin(false) }
    }

    async function loginByGmail(){
        //login by gmail
        console.log(1)
    }

    return (
        <div className="body">
        <div className="screenLogin">
            <div className="left">
                <div className="area-logo">
                    <img src={require("..\\public\\Hanase2.png")}></img>
                </div>
            </div>
            <div className="rigth">
                <div className="area-Login">
                    <div className="area-logo">
                        <img src={require("..\\public\\Artboard 3.png")}></img>
                    </div>
                    <div>
                        <div onClick={() => loginByGmail()}>login gmail</div>
                    </div>
                    <div className="form-Login">
                        <div className="area-statusLogin">
                            {statusLogin ? <div /> : <div className="warning">
                                Invalid username/password </div>}
                        </div>
                        <form>
                            <div className="area-Input">
                                <div className="field">User name:</div>
                                <input
                                    className="inputField"
                                    type={"text"}
                                    onChange={(username) => setUsername(username.target.value)}
                                ></input>
                            </div>
                            <div className="area-Input">
                                <div className="field">Password:</div>
                                <input
                                    className="inputField"
                                    type={"password"}
                                    onChange={(password) => setPassword(password.target.value)}
                                ></input>
                            </div>
                            <div className="area-fgPass">
                                <div className="forgetPass">Forgot password?</div>
                            </div>
                            <div className="area-status">
                                {username ? <div className="right">*Fill field Username</div> : <div className="warning">*Fill field Username</div>}
                                {password ? <div className="right">*Fill field Password</div> : <div className="warning">*Fill field Password</div>}
                            </div>
                            <div className="area-btnLogin">
                                {password && username ?
                                    <div className="button-Login" onClick={loginSystem}>
                                        Login
                                    </div> :
                                    <div className="button-LoginDisable" e>
                                        Login
                                    </div>
                                }
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

export default Login;
